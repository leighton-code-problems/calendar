const { src, dest } = require('gulp');
var sass = require('gulp-sass');

exports.default = function() {
    return src('assets/scss/main.scss')
        .pipe(sass())
        .pipe(dest('assets/css'));
  }