let date = new Date();

let currentDay;
let eventDay;
let eventId;
let currentMonth = date.getMonth();
let currentYear = date.getFullYear();
let appointments = [];
let appointmentsToAdd;
let apptForm = document.getElementById("appt-form");
let dayElements = document.getElementsByClassName("day");
const appointmentMenu = document.getElementById('appointment-menu');
const eventInfo = document.getElementById('event-info');
const apptStartTime = document.getElementById("appointment-time-start");
const apptEndTime = document.getElementById("appointment-time-end");

document.getElementById("btn-prev-month").addEventListener("click", prevMonth);
document.getElementById("btn-next-month").addEventListener("click", nextMonth);
document.getElementById("btn-close-appt-menu").addEventListener("click", closeApptMenu);
document.getElementById("btn-close-event-info").addEventListener("click", closeEventInfo);
document.getElementById("btn-delete-event").addEventListener("click", deleteEvent);
apptStartTime.addEventListener("input", updateMinValue);

function init() {
    generateDays(date.getFullYear(), date.getMonth());
    loadFromLocalStorage();
    if (appointments.length > 0) {
        loadCalendarEvents();
    }
}

function generateDays(year, month) {
    let currentWeek = 0;
    const firstDayOfMonth = new Date(year, month, 0).getDay();
    let daysInMonth = new Date(year, month +1, 0).getDate();
    let day = firstDayOfMonth +1;
    let calDate = 1;

    updateTopBar(year, month);


    // Removes all instances of days
    let calDayCollection = document.getElementsByClassName('cal-day');
    for (var i = 0, len = calDayCollection.length; i < len; i++) {
        calDayCollection[0].parentNode.removeChild(calDayCollection[0]);
    }
    // Removes all instances of appointments
    let apptCollection = document.getElementsByClassName('calendar-event');
    for (var i = 0, len = apptCollection.length; i < len; i++) {
        apptCollection[0].parentNode.removeChild(apptCollection[0]);
    }

    let dayCollection = document.getElementsByClassName('day')
    for (var i = 0, len = dayCollection.length; i < len; i++) {
        dayCollection[i].dataset.day = '';
    }

    for(i=0; i < daysInMonth; i++) {
        if (day == 8) {
            currentWeek = currentWeek + 1;
            day = 1;
        }
        let element = document.getElementsByClassName('day-0' + day)[currentWeek];
        element.innerHTML = '<div class="cal-day">' + calDate + "</div>";
        element.dataset.day = calDate;
        day = day + 1;
        calDate = calDate + 1;
    }
    
    if (currentWeek != 5) {
        document.getElementById('week-6').classList.add('no-show');
    } else {
        document.getElementById('week-6').classList.remove('no-show');
    }
}

function updateTopBar(year, month) {
    let displayedDate = new Date(year, month + 1, 0);  // 2009-11-10
    let displayedMonth = displayedDate.toLocaleString('default', { month: 'long' });
    document.getElementById('current-month').innerHTML= displayedMonth;
    document.getElementById('current-year').innerHTML= currentYear;
}

Array.from(dayElements).forEach(function(element) {
    element.addEventListener('click', openApptMenu);
});

function eventClickHandle() {
    let eventElements = document.getElementsByClassName("calendar-event");
    Array.from(eventElements).forEach(function(element) {
        element.addEventListener('click', openEvent);
    });
}

function openApptMenu(e) {
    if (e.target.classList.contains('day') || e.target.classList.contains('cal-day') ) {
        currentDay = this.dataset.day;
    
        if (currentDay != '') {
            closeEventInfo();
            let x = event.clientX;
            let y = event.clientY;
            let topPos = y;

            appointmentMenu.style.display = 'block';

            if ((appointmentMenu.clientHeight + y) > document.getElementsByTagName('body')[0].clientHeight) {
                topPos = (document.getElementsByTagName('body')[0].clientHeight - appointmentMenu.clientHeight);
            }
            
            appointmentMenu.style.top =  topPos + "px";
            appointmentMenu.style.left =  x + "px";
        }
    }
}

function closeApptMenu() {
    appointmentMenu.style.display = '';
    apptForm.elements[0].value = '';
    apptForm.elements[1].value = '';
    apptForm.elements[2].value = '';
    apptForm.elements[3].value = '';
}

function prevMonth() {
    if (currentMonth == 0) {
        currentMonth = 11;
        currentYear = currentYear - 1;
    } else {
        currentMonth = currentMonth - 1;
    }

    generateDays(currentYear, currentMonth);
    loadCalendarEvents();
}

function nextMonth() {
    if (currentMonth == 11) {
        currentMonth = 0;
        currentYear = currentYear + 1;
    } else {
        currentMonth = currentMonth + 1;
    }

    generateDays(currentYear, currentMonth);
    loadCalendarEvents();
}

function updateMinValue() {
    apptEndTime.setAttribute("min", apptStartTime.value);
}

apptForm.addEventListener("submit", function(event) {
    event.preventDefault();
    let appt = {
        'id' : Math.floor(Math.random() * (99999999 - 0)),
        'title' : apptForm.elements[0].value,
        'startTime' : apptForm.elements[1].value,
        'endTime' : apptForm.elements[2].value,
        'description' : apptForm.elements[3].value,
        'dayDate' : parseInt(currentDay, 10),
        'monthDate' : currentMonth,
        'yearDate' : currentYear
    }
    appointments.push(appt);
    saveToLocalStorage();
    closeApptMenu();
    addCalendarEvent(parseInt(currentDay, 10), appt);
});

function addCalendarEvent(day, appointment) {
    eventDay = document.querySelectorAll("[data-day='" + day + "'");
    eventDay[0].innerHTML += `<div class="calendar-event" data-id="${appointment.id}">
    <div class="eventTime">${appointment.startTime} ${(appointment.endTime != '' ? " - " + appointment.endTime : '')}</div>
    <div class="eventTitle">${appointment.title}</div>
    </div>`;
}

function openEvent(e) {
    closeApptMenu();
    let x = event.clientX;
    let y = event.clientY;
    let topPos = y;

    eventId = e.target.parentElement.dataset.id;

    for (i=0;i < appointments.length; i++) {
        if (appointments[i].id == eventId) {
            document.getElementById("event-info-title").innerHTML = isNotBlank(appointments[i].title);
            document.getElementById("event-info-desc").innerHTML = isNotBlank(appointments[i].description);
            document.getElementById("event-info-start-time").innerHTML = isNotBlank(appointments[i].startTime);
            document.getElementById("event-info-end-time").innerHTML = (appointments[i].endTime != '' ? " - " + appointments[i].endTime : '');
        }
    }

    eventInfo.style.display = 'block';

    if ((eventInfo.clientHeight + y) > document.getElementsByTagName('body')[0].clientHeight) {
        topPos = document.getElementsByTagName('body')[0].clientHeight - eventInfo.clientHeight;
    }

    eventInfo.style.top =  topPos + "px";
    eventInfo.style.left =  x + "px";
}

function deleteEvent() {
    console.log(appointments);
    for (i=0;i < appointments.length; i++) {
        if (appointments[i].id == eventId) {
            appointments.splice(i, 1);
            console.log(appointments);
            closeEventInfo();
            saveToLocalStorage();
            // Removes event element from page
            let apptCollection = document.getElementsByClassName('calendar-event');
            for (var i = 0, len = apptCollection.length; i < len; i++) {
                if (apptCollection[i].dataset.id == eventId) { 
                    apptCollection[i].parentNode.removeChild(apptCollection[i]);
                }
            }
        }
    }
}

function isNotBlank(toCheck) {
    return (toCheck != '' ? toCheck : '');
}

function closeEventInfo() {
    eventInfo.style.display = '';
    document.getElementById("event-info-title").innerHTML = "";
    document.getElementById("event-info-desc").innerHTML = "";
    document.getElementById("event-info-start-time").innerHTML = "";
    document.getElementById("event-info-end-time").innerHTML = "";
    // requestNotifications();
}

function loadCalendarEvents() {
    appointmentsToAdd = [];

    for (i = 0; i < appointments.length; i++) {
        if (appointments[i].monthDate == currentMonth) {
            appointmentsToAdd.push(appointments[i]);
        }
    }
    
    if (appointmentsToAdd.length > 0) {
        addCalendarEvents();
    }
}

function addCalendarEvents() {
    for (i = 0; i < appointmentsToAdd.length; i++) {
        addCalendarEvent(appointmentsToAdd[i].dayDate, appointmentsToAdd[i]);
    }

    eventClickHandle();
}

function saveToLocalStorage() {
    localStorage.setItem('localCalendar', JSON.stringify(appointments));
}

function loadFromLocalStorage() {
    if (localStorage.getItem('localCalendar') != null) {
        appointments = JSON.parse(localStorage.getItem('localCalendar'));
    }
}

// function requestNotifications() {
//     if (Notification.permission !== "granted") {
//         console.log('something');
//         Notification.requestPermission();
//     } else {
//         var notification = new Notification('hello', {
//             body: "Hey there!",
//           });
//     }
// }

init();